#/bin/sh

#データベース接続設定。スレーブサーバーに接続する
DBHOST="localhost"
DBUSER="root"
DBPASS=""

#mysqlコマンドのフルパスと指定と、一時ファイルの指定
MYSQL="/usr/bin/mysql"
resulttmp="tmp.$$"

#SHOW SLAVE STATUSをMySQLサーバーに問い合わせて、一時ファイルに出力
$MYSQL -h "${DBHOST}" -u "${DBUSER}" -p"{DBPASS}" -e "SHOW SLAVE STATUS
\G" > $resulttmp

#レプリケーション状態に関するパラメータを抽出
Slave_IO_Running=$(awk '/Slave_IO_Running:/ {print $2}' "$resulttmp")
Slave_SQL_Running=$(awk '/Slave_SQL_Running:/ {print $2}' "$resulttmp"}
Last_IO_Error=$(awk 'Last_IO_Error:' "$resulttmp" | sed 's/^ *//g')
Last_SQL_Error=$(awl 'Salt_SQL_Error:' "$resulttmp" | sed 's/^ *//g')

#現在日付を 2013/02/01 13:15:44 の形で組み立てて用意
date_str=$(date '+%Y/%m/%d %H:%M:%S')

#Slace_IO_RunningとSlace_SQL_Runningが共にYESでなければエラー
if [ "$Slave_IO_Running" = "YES" -a "$Slave_SQL_Running" = "YES" ]; then
	echo "[$date_str] STATUS OK"
else
	echo "[$date_str] STATUS NG"
	echo "Slave_IO_Running: $Slave_IO_Running"
	echo "Slave_SQL_Running: $Slave_SQL_Running"
	echo "$Last_IO_Error"
	echo "$Last_SQL_Error"
	
	#警告メール送信などのアラートをあげる
	./alert.sh
fi

#一時ファイルの削除
rm -f "$resulttmp"

