#!/bin/sh

#指定されたリストファイルの存在チェック
if [ ! -f "$1" ]; then
	echo "対象のパッケージリストファイルは存在しません: $1" >&2
	exit 1
fi

#引数で指定されたファイルから、パッケージリストを取得
pkglist=$(cat "$1")

#インストール済みのrpmの更新日付を出力する
rpm -q $pkglist --queryformat '%{INSTALLTIME:date} : %{NAME}\n'
