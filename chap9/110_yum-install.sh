#!/bin/sh

#インストールするパッケージ名の定義
pkglist="httpd zsh xz git"

#パッケージリストから順に１行ずつ読み込み
for pkg in $pkglist
do
	#yumコマンドでパッケージをインストールする
	yum -y install $pkg
done
