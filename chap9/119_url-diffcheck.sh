#/bin/sh

#監視対象のURL
url="http://yahoo.co.jp"

#ダウンロードファイルのファイル名定義
newfile="new.dat"
oldfile="old.dat"

#ファイルをダウンロード
curl -so "$newfile" "$url"

#前回ダウンロードしたファイルと今ダウンロードしたファイルを比較
cmp -s "$newfile" "$oldfile"

#cmpコマンドの終了ステータスが０でないなら、差があったといえる
if [ $? -ne 0 ]; then
	#現在日付を[2013/02/01 13:15:44]の形で組み立て
	date_str=$(date '+%Y/%m/%d %H:%M:%S')

	#ファイル変更の通知
	echo "[$date_str] 前回ダウンロード時からファイルの変更がありました"
	echo "対象URL: $url"
	./alert.sh
fi

#ダウンロードしたファイルをリネームして保存
mv -f "$newfile" "$oldfile"
