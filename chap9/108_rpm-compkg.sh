#!/bin/sh

#ファイルを指定するコマンドライン引数をチェック
if [ ! -f "$1" ]; then
	echo "ファイルがありません" >&2
	exit 2
fi

#ファイル名から、属するrpmパッケージ名を取得する
pkgname=$(rpm -qf "$1")

#rpm -qf コマンドの結果でパッケージ名を表示する
if [ $? -eq 0 ]; then
	echo "$1 -> $pkgname"
else
	echo "$1 はパッケージに属していません" >&2
	exit 1
fi

