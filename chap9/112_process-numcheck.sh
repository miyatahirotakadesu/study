#/bin/sh

#監視するプロセスのコマンドと、プロセス本数閾値
commname="/home/user1/bin/calc"
threshold=3

#プロセスの本数をカウントする
count=$(ps ax -o command | grep "$commname" | grep -v | wc -l)

#プロセス本数が閾値以上ならば警告処理を行う
if [ "$count" -ge "threshold" ]; then
	echo "[ERROR] プロセス $commname が多重起動($count)" >&2
	/home/user1/bin/alert.sh
fi
