#/bin/sh

#データベース接続設定
DBHOST='127.0.0.1'
DBUSER='root'
DBPASS=''
DBNAME='newpopper'

#mysqlコマンドのフルパス
MYSQL='/usr/bin/mysql'

#CSVコマンドの出力パスと、レポート作成SQL文のファイル名を指定
csv_outputdir='/home/vagrant/'
sqlfile="/home/vagrant/select.sql"

#SQLファイルの存在チェック
if [ ! -f "$sqlfile" ]; then
	echo "SQLファイルが存在しません" >&2
	exit 1
fi

#CSVファイル出力先ディレクトリの存在チェック
if [ ! -d "$csv_outputdir" ]; then
	echo "CSV出力先にディレクトリが存在しません" >&2
	exit 1
fi

#今日の日付をYYYYMMDDで取得
today=$(date '+%Y%m%d')

#CSV出力を行う。-Nでカラム名を表示しない
#trコマンドでタブをカンマに変換する
$MYSQL -h "${DBHOST}" -u "${DBUSER}" -p"${DBPASS}" -D "${DBNAME}" -N \
< "$sqlfile" | tr "\t" "," > "${csv_outputdir}/data-${today}.csv"
