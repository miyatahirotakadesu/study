#/bin/sh
#監視するCPUのidleの閾値。この値以下ならば警告
idle_limit=10.0

#CPUの%idleをmpstatコマンドで取得。最終行の平均値を取り出す
cpu_idle=$(mpstat 1 5 | tail -n 1 | awk '{print $NF}')

#現在の%idleと閾値をbcコマンドで比較
is_alert=$(echo "$cpu_idle" < "$idle_limit" | bc)

#警告アラートを上げるか判断
if [ "$is_alert" -eq 1 ]; then
	#現在日付を[2013/02/01 13:15:44]の形で組み立て
	date_str=$(date '+%Y%m%d  %H:%M:%S')

	#CPU %idle 低下の警告出力
	echo "[$date_str] CPU %idle Alert: $cpu_idle %"
	./alert.sh
fi
