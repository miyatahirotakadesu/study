#/bin/sh

#監視するディスク使用量の閾値パーセンテージ
used_limit=9
#dfコマンドの出力結果の一時ファイル名
tmpfile="df.tmp.$$"

#dfコマンドでディスク使用量を表示。1行目はゲッダなので除外する
df -P | awk 'NR >= 2 {print $5,$6}' > "$tmpfile"

#dfコマンドの出力の一時ファイルから、使用率を確認する
while read percent mountpoint
do
	#"31%" を "31" に。末尾の記号を削除する
	percent_val=${percent%\%}

	#ディスク使用量が規定値以上ならばアラート
	if [ "$percent_val" -ge "$used_limit" ]; then
		#現在日付を[2013/02/01 13:15:44]の形で組み立て
		date_str=$(date '+%Y/%m/%d %H:%M:%S')

		echo "[$date_str] Disk Capacity Alert: $mountpoint ($percent used)"
		./alert.sh
	fi
done < "$tmpfile"

#一時ファイル削除
rm $tmpfile
