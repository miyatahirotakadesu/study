#!/bin/sh

#ifconfigコマンドで有効なインタフェースを表示して、
#awkコマンドでインタフェース名とIPアドレスを抽出する
LANG=C /sbin/ifconfig |\
awk '/^[a-z]/ {print "[" $1 "]"}
/inet / {split($2,arr,":"); print arr[2]}'
