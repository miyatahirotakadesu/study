#/bin/sh

#監視対象のログファイル名を設定
logfile="/var/log/httpd/access_log"

#tailコマンドでログ監視
# * -F リアルタイムに監視
# * -n 0 追記ぶんのみを対象とする
tail -F -n 0 "$logfile" |\
while read line
do
	#ログにマッチする文字列があれば警告表示をする
	case "$line" in
		*"Not Found"*)
			echo "!注意!　ファイルが見つかりません： $line"
			;;
		*"Application Error"*)
			echo "!警告! アプリケーション異常 ： $line"
			;;
		*"192.168"*)
			echo "!警告! ローカルアクセス ： $line"
			;;
	esac
done
