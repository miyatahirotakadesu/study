#!/bin/sh

#自分以外のユーザーがログインしていないかを whoコマンドの出力からチェック
other_user=$(who | wc -l)
if [ "$other_user" -ge 2 ]; then
	echo "[ERROR]whoコマンドの出力が２行以上：作業中のユーザーがいます" >&2
	exit 1
fi

#事前に停止しておくべきプロセスが、起動したままでないかチェックする
commname="/usr/libexec/mysqld"
ps ax -o command | grep -q "^$commname"
if [ $? -eq 0 ]; then
	echo "[ERROR] シャットダウンを中止：プロセス$commnameが起動中" >&2
	exit 2
fi

#シャットダウンを実行
shutdown -h now
