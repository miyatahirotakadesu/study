#/bin/sh

#監視対象URLを指定する
url="http://yahoo.co.jp"

#現在日付[2013/02/01 13:15:44]の形で組み立て
date_str=$(date '+%Y/%m/%d %H:%M:%S')

#監視URLにcurlコマンドでアクセスし、終了ステータスを変数curlresultに代入
httpstatus=$(curl -s "$url" -o /dev/null -w "%{http_code}")
curlresult=$?

#curlコマンドが失敗していればHTTP接続自体が異常とみなす
if [ "$curlresult" -ne 0 ]; then
	echo "[$date_str] HTTP接続異常:curl exit status[$curlresult]"
	./alert.sh
elif [ "$httpstatus" -ge 400 ]; then
	echo "[$date_str] HTTPステータス異常:HTTP status[$httpstatus]"
	./alert.sh
fi
