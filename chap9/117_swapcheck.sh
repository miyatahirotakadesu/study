#/bin/sh

#監視するスワップ発生回数。これ以上ならば警告する
swapcount_limit=10

#vmstatコマンドの出力から　スワップイン・スワップアウトの値を取得する
swapcount=$(vmstat 1 6 | awk 'NR >= 4 {sum += $7 + $8} END{print sum;}')

if [ "$swapcount" -ge "$swapcount_limit" ]; then
	#現在日付を[2013/02/01 13:15:44]の形で組み立て
	date_str=$(date '+%Y/%m/%d %H:%M:%S')

	#スワップ発生の警告出力
	echo "[$date_str] Swap Alert: $swapcount (si+so)"
	./alert.sh
fi
