#!/bin/sh

#このスクリプトの実行を許可するユーザーの定義
script_user="batch1"

#idコマンドで現在のユーザーを取得し、定義と一致するか確認
if [ $(id -nu) = "$script_user" ]; then
	#許可なら実行
	./batch_program
else
	echo "[ERROR] $script_user ユーザーで実行してください" >&2
	exit 1
fi
