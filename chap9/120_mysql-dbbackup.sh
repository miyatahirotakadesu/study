#/bin/sh

#データベース接続設定
DBHOST="localhost"
DBUSER="root"
DBPASS=""
DBNAME="develop_akiba_dmm_make"

#データベースバックアップ設定
BACKUP_DIR="/home/vagrant/bk/"
BACKUP_ROTATE=3
MYSQLDUMP="/usr/bin/mysqldump"

#バックアップ出力先ディレクトリのチェック
if [ ! -d "$BACKUP_DIR" ]; then
	echo "バックアップ出力先ディレクトリが存在しません： $BACKUP_DIR" >&2
	exit 1
fi

#今日の日付をYYYYMMDDで取得
today=$(date '+%Y%m%d')

#mysqldumpコマンドでデータベースのバックアップ取得
$MYSQLDUMP -h "${DBHOST}" -u "${DBUSER}" "${DBNAME}" > "${BACKUP_DIR}/${DBNAME}-${today}.dump"

#mysqldumpコマンドの終了ステータスで成功・失敗を確認
if [ $? -eq 0 ]; then
	gzip "${BACKUP_DIR}"/${DBNAME}-${today}.dump
	#古いバックアップを削除する
	find "$BACKUP_DIR" -name "${DBNAME}-*.gz -mtime +${BACKUP_ROTATE}" | xargs rm -f
else
	echo "バックアップ作成失敗: ${BACKUP_DIR}/${DBNAME}-${today}.dump"
	exit 2
fi
