#!/bin/sh

#tar tf ~~.tar 既存ファイルの確認ができる

#年月でアーカイブファイルを指定（例：201412.tar）
archivefile="$(date +'%Y%m').tar"
#今日の日付からログファイル指定
logfile="$(date +'%Y%m%d').log"

#月次アーカイブに、今日のログを追加
tar rvf $archivefile log/$logfile
