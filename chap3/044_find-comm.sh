#!/bin/sh

dirA="dir1"
dirB="dir2"

#dir1/とdir2/のファイルリストの差を調べる
( cd ${dirA}; find . -maxdepth 1 -type f -print | sort ) > tempfile1.lst
( cd ${dirB}; find . -maxdepth 1 -type f -print | sort ) > tempfile2.lst

comm tempfile1.lst tempfile2.lst
#commコマンド
#１列目　1ファイル目のみの行を表示
#２列目　2ファイル目のみの行を表示
#３列目　両方に含まれてる行を表示
