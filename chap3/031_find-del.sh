#!/bin/sh

logdir="."

#最終更新日時が１年以上前の古いファイル削除
find $logdir -name "*.log" -mtime +364 | xargs rm -fv

#touch -t で特定日時のファイルを作れる
#空白含むファイル名のためには xargs -0 にする
find $logdir -name "*.log" -mtime +364 -print0 | xargs -0 rm -fv
