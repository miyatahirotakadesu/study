#!/bin/sh
#statコマンドでタイムスタンプを見れる stat ファイル名

timestamp="201311190123.45"

touch -t $timestamp app1.log
touch -c -t $timestamp lock.tmp
#-c オプションをつけると、ファイルが無い場合はファイル新規作成されない
