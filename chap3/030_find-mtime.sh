#!/bin/sh

logdir="./"

#４日前から２日前までに更新されたファイル一覧を表示
find $logdir -name '*.sh' -mtime -4 -mtime +1 -print
#mtime -2 48時間前〜現在
#mtime 2  72時間前〜48時間前
#mtime +2 〜72時間前 プラスはまぎわらしい
