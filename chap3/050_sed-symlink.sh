#/bin/sh

filename="target.txt"

if [ ! -e "$filename" ]; then
	#対象ファイルが存在しなければ終了
	echo "ERROR: file not exitst." >&2
	exit 1
elif [ -h "$filename" ]; then
	#対象ファイルがシンボリックリンクならば readlinkコマンドで
	#実体ファイルに対して処理を行う
	sed -i.bak "s/Hello/hi/g" "($readlink "$filename")"
else
	sed -i.bak "s/Hello/hi/g" "$filename"
fi
