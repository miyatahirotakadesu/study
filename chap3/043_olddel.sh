#!/bin/sh

#新旧を比較するファイル
log1="log1.log"
log2="log2.log"

#引数のファイルが存在するかを調べ、存在しない場合は終了する
filecheck()
{
	if [ -e "$1" ]; then
		echo "ERROR: File $1 does not exist." >&2
		exit 1;
	fi
}

filecheck "$log1"
filecheck "$log2"

#2つのファイルの新旧を比べ、古い方を削除する
if [ "$log1" -nt "$log2" ]; then
	echo "[$log1]->newer, [$log2]->older"
	rm $log2
else
	echo "[$log2]->newer, [$log1]->older"
	rm $log1
fi
