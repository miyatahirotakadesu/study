#/bin/sh

#IFSに改行を設定する
IFS='
'

#カレントディレクトリ配下のファイルを$filename として順に処理
for filename in $(ls -AF)
do
	case "$filename" in
		.*/)
			echo "dot directory: $filename"
		;;
		.*)
			echo "dot file: $filename"
		;;
	esac
done

