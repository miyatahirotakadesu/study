#!/bin/sh

#コマンドライン引数の上限値を確認
getconf ARG_MAX

logdir="/var/log"

find $logdir -name "*.log" -print | xargs sudo grep "ERROR" /dev/null
