#!/bin/sh
backup_dir="~/backup/"

#myappディレクトリを$backup_dir下にコピー
while getopts "a" option
do
	case $option in
		a)
			cp -a log "$backup_dir"
			exit
			;;
	esac
done

cp -R myapp "$backup_dir"
