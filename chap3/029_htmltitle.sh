#!/bin/sh

#カレントディレクトリの.htmlファイルを対象
for htmlfile in *.html
do	
	#basenameコマンド　$htmlfile　の.htmlをとってファイル名表示
	fname=$(basename $htmlfile .html)

	#<title></titleを後方参照\1として抽出、ファイル出力
	sed -n "s/^.*<title>\(.*\)<\/title>.*$/\1/p" $htmlfile > $fname.txt
	#-nが無いと、全ての行を出力してしまう。
	#/pで、置き換えの起こった行のみを出力する
done
