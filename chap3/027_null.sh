#!/bin/sh
#uptimeコマンドの記録ファイルを定義
uptimelog="uptimelog.log"

#ヌルコマンドで空ファイルに初期化する
#touchコマンドでは、ファイルが存在した場合更新日を上書きするだけになる
: > $uptimelog
#/dev/null > $uptimelog でもよい
#true > $uptimelog でもよい

for i in `seq 1 6`
do
	uptime >> $uptimelog
	sleep 10
done
