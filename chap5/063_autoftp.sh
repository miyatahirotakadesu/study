#!/bin/sh

#FTP接続のための設定
server="127.0.0.1"
user="vagrant"
password="vagrant"
dir="/home/user1/myapp/log"
filename="app.log"

ftp -n "$server" << __EOT__
user "$user" "$password"
binary
cd "$dir"
get "$filename"
__EOT__
