#/bin/sh

while read ipaddr
do
	#hostコマンドでIPアドレスを逆引きする
	revlookup=$(host "$ipaddr")

	#hostコマンドの逆引きが成功したかどうか
	if [ $? -eq 0 ]; then
		echo -n "$ipaddr"
		echo "$revlookup" | awk '{print $NF}' | sed 's/\.$//'
	else
		echo "$ipaddr,"
	fi


	#DNSサーバー負荷軽減のため１秒まつ
	sleep 1

done < $1
