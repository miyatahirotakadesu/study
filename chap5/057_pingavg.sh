#!/bin/sh

ipaddr="192.168.2.1"
count=2

echo "Ping to: $ipaddr"
echo "Ping count: $count"
echo "Ping AVG[ms]:"

#$$にはプロセスIDが入る。一時ファイル名としてよく使われる。

for i in `seq 0 $count`
do
		ping -c 1 $ipaddr >> ping.$$
done

#"time=4.32ms" の部分をsedで取り出し、awkで平均値を集計する。
#sed -n オプションでパターンスペースの内容を表示しない　＆　pフラグで置換が発生した行だけ出力→結果、数値部分のみ出力
cat ping.$$
sed -n "s/^.*time \(.*\)ms/\1/p" ping.$$ |\
#パイプ＆改行のため\でつなぐ
awk '{sum+=$1} END{print sum/NR}'

#一時ファイルの削除
rm -f ping.$$
