#/bin/sh
ipaddr="192.168.2.52"
faillog="fail-port.log"

#テストするポートは 80,2222,8080
for port in 80 2222 8080
do
	nc -w 5 -z $ipaddr $port
	if [ $? -ne 0 ]; then
		echo "Failed at port: $port" >> "$faillog"
	fi
done
