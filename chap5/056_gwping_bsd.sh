#!/bin/sh
#netstatコマンドからデフォルトゲートウェイ取得
gateway=$(netstat -nr | awk '$1 == "default" {print $2}')
ping -c 1 $gateway > /dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "success"
else
	echo "failed"
fi
