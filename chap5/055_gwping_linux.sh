#!/bin/sh
#デフォルトゲートウェイ = 外部ネットワークとの出入り口になる機器。一般的にルータ
#route -n でホスト名でなく、IPでルーティングテーブル表示
gateway=$(route -n | awk '$1 == "0.0.0.0" {print $2}')
echo "$gateway"
ping -c 1 $gateway > /dev/null 2>&1
#特殊シェル変数$?が0かどうかで成功、失敗を判断
if [ $? -eq 0 ]; then
	echo "[Success] ping -> $gateway"
else
	echo "[Failed] ping -> $gateway"
fi
