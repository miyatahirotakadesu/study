#!/bin/sh

ipaddr="10.0.2.2"

macaddr=$(arp -an | awk "/\($ipaddr\)/{print \$4}")

if [ -n $macaddr ]; then
	echo "$ipaddr -> $macaddr"
else 
	echo "$ipaddrは ARPキャッシュ にありません"
fi
