#/bin/sh

#転送速度を測る一時ファイルのサイズ指定。単位はKB
filesize=1024

#転送速度を測る一時ファイルのファイル名
tmpdata="tmpdata.tmp"
timefile="timecount.tmp"

#転送に用いる一時ファイルを作成する
dd if=/dev/zero of="$tmpdata" count=$filesize bs=1024

#FTP接続してファイルをPUTする
server="192.168.2.5"
user="user1"
pasword="xxx"

echo "Filesize: $filesize KB"
echo "FTP Server: $server"

(time -p ftp -n "$server") << __EOT__ 2> "$timefile"
time ftp -n "$server" << __EOT__
user "$user" "$pasword"
binary
put "$tmpdata"
__EOT__

#timeコマンドの出力結果から実時間を取得して
#割り算して速度を求める
realtime=$(awk '/^real / {print $2}' "$timefile")
speed=$(echo "${filesize}/${realtime}" | bc)

echo "Transfer Speed: $speed (KB/sec)"

#転送一時ファイルの削除
rm -f "$tmpdata" "$timefile"
