#/bin/sh

#疎通確認対象サーバー
checkserver="192.168.2.35"

#スクリプトを実行するホスト名を表示する
hostname

#サーバーへの疎通をpingコマンドで確認する
ping -c 1 $checkserver > /dev/null 2>&1

if [ $? -eq 0 ]; then
	ecoh "Ping to $checkserver : [OK]"
else
	echo "Pint to $checkserver : [NG]"
fi
