#!/bin/sh

#IPアドレスを取得したいホスト名を定義
fqdn="www.google.com"

echo "Address of $fqdn"
echo "==============="

#hostコマンドでip取得、awkで加工して出力
host $fqdn | \
awk '/has address/ {print $NF,"IPv4"} \
/has IPv6 address/ {print $NF,"IPv6"}'

