#/bin/sh

#変換後のファイル出力先ディレクトリ名
outdir="newdir"

#ファイル出力先のディレクトリのチェック
if [ ! -d "$outdir" ]; then
	echo "Not a directory: $outdir"
	exit 1
fi

#カレントディレクトリの.htmlファイルを対象
for filename in *.html
do
	#grepコマンドでmetaタグのContent-Type行を選択肢、
	#sedコマンドでcharset=指定部分を抜き出す
	charset=$(grep -i '<meta ' "$filename" |\
	grep -i 'http-equiv="Content-Type"' |\
	sed -n 's/.*charset=\([-_a-zA-Z0-9]*\)".*/\1/p')

	#charsetが取得出来ていない場合は、iconvコマンドを実行せずにスキップする
	if [ -z "$charset" ]; then
		echo "charset not found: $filename >&2"
		continue
	fi

	#metaタグから取り出した文字コードから、UTF-8へと変換し、
	#ディレクトリ$outdirに出力する
	iconv -c -f "$charset" -t UTF-8 "$filename" > "${outdir}/${filename}"
done
