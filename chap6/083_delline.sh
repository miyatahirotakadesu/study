#/bin/sh

#変換後のファイル出力用ディレクトリ名
outdir="newdir"

#ファイル出力用ディレクトリのチェック
if [ ! -d "$outdir" ]; then
	echo "Not a directory: $outdir"
	exit 1
fi

for filename in *.js
do
	#空行およびスペースやタブのみの行を、sedコマンドのdで削除
	sed '/^[[:blank:]]*$/d' "$filename" > "${outdir}/${filename}"
done
