#!/bin/sh


#一時ファイルの指定
tmpfile="sort.lst"

#対象IDファイルを確認する
if [ ! -f "$1" ]; then
	echo "IDリストファイルを指定してください" >&2
	exit 1;
fi

# IDの末尾の数字でリストをソートする
rev "$1" | sort | rev > $tmpfile

./report.sh $tmpfile

#一時ファイルの削除
rm $tmpfile
