#ruby

#事前設定しておかないとエラーになる環境変数の定義
envname="ZSH"

#envコマンドで環境変数一覧を表示し、cutコマンドで
# * 1番目の値を表示 [-f 1]
# * 区切り文字記号は: [-d "="]
# として表示する

env | cut -f 1 -d "=" > env.list

#チェックする環境変数名がenv.listにマッチするかどうかで
#未定義かどうか確認する
grep -q "^${envname}$" env.list

if [ $? -eq 0 ]; then
	#環境変数が設定されていればstart.shを実行
	echo "環境変数 $envname は設定されています"
	./start.sh
else
	echo "環境変数 $envname は設定されていません"
fi
