#!/bin/sh

csvfile="data.csv"
GRAPH_WIDTH=50

markprint() {

	local i=0
	while [ $i -lt $1 ]
	do
		echo -n "*"
		i=$(expr $i + 1)
	done

}

#データから最大値取得
max=$(awk -F, '{print $3}' "$csvfile" | sort -nr | head -n 1)

#データのすべてが0の場合は最大値を1とする
if [ $max -eq 0 ]; then
	max=1
fi


#CSVファイルを読み込み、値ごとの値をグラフ出力する
while IFS=, read id name score
do
	markprint $(expr $GRAPH_WIDTH \* $score / $max)
	echo " [$name]"
done < $csvfile

