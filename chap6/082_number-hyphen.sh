#/bin/sh

#ハイフンを削除するかどうかのフラグ。１ならば削除する
d_flag=0

#getoptsコマンドで、削除オプション(-d)を追加
while getopts "d" option
do
	case $option in
		d)
			d_flag=1
			;;
		\?)
			exit 1
			;;
	esac
done

#コマンドライン引数で指定された郵便番号ファイルを、
#シェル変数filenameに代入する
shift $(expr $OPTIND - 1)
filename="$1"

#指定された郵便番号ファイルの存在チェック
if [ ! -f "$filename" ]; then
	echo "対象のファイルが存在しません: $filename" >&2
	exit 1
fi

#d_flagが指定されていればハイフンを削除、されていなければハイフン付与
if [ "$d_flag" -eq 1 ]; then
	#ハイフンを削除する
	#awkで前後スペース除去→フォーマットチェック→ハイフン削除
	awk '{print $1}' "$filename" | grep '^[0-9]\{3\}-[0-9]\{4\}$' | sed "s/-//"
else
	#ハイフンを付加する
	#awkで前後スペース除去→フォーマットチェック→ハイフン付与
	awk '{print $1}' "$filename" | grep '^[0-9]\{7\}$' | sed "s/\(...\)/\1-/"
fi
