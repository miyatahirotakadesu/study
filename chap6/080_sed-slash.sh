#!/bin/sh

#出力ディレクトリの定義
outdir="newdir"

#出力ディレクトリの存在チェック。無ければ終了
if [ ! -d "$outdir" ]; then
	echo "出力ディレクトリがありません：$outdir" >&2
	exit 1
fi

#カレントディレクトリのhtmlファイルを処理
for htmlfile in *.html
do
	#ファイル内のテキストで/img/というパスを/images/に変換する
	sed "s%/img/%/images/%g" "$htmlfile" > "${outdir}/${htmlfile}"
done
