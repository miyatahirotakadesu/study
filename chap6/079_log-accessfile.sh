#!/bin/sh

logfile="/var/log/httpd/access_log"

#ログファイルが存在しなければ終了
if [ ! -f "$logfile" ]; then
	echo "対象のログファイルは存在しません"
	exit 1
fi

#ログファイルから、GETメソッドで取得されたファイルのアクセス回数を集計する
#awkコマンドでファイルを取り出し、sort + uniq でカウント後に降順ソートする
awk '$6=="\"GET" {print $7}' "$logfile" | sort | uniq -c | sort -nr
