#!/bin/sh

#CSVファイルが存在しなければ終了
if [ ! -f "$1" ]; then
	echo  "対象のCSVファイルが存在しません: $1" >&2
	exit 1
fi

#拡張子を除いたファイル名を取得
filename=${1%.*}

awk -F, '{sum += $3} END{print sum / NR}' "$1"

