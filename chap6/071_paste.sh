#!/bin/sh

#ハッシュ値を出力する一時ファイルを初期化する
tmpfile="hash.txt"
: > $tmpfile

#シェルの区切り文字を改行のみとする
IFS='
'

#指定されたテキストファイルから一行ずつ読み込む
while read -r line
do
	#各行のMD5ハッシュを取得する。
	#コマンドの後ろにはファイル名が着くため、１カラム目を取り出す
	echo -n "$line" | md5sum | awk '{print $1}' >> $tmpfile
done < $1

#元のテキストファイルと、ハッシュ値出力した一時ファイルを
#カンマ区切りで連結して表示する
paste -d, "$1" $tmpfile
