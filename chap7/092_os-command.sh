#/bin/sh

#echoコマンドのパスを、環境によって変えて
#シェル変数ECHOに代入する
case $(uname -s) in
	#Macの場合はシェルビルトインではなく /bin/echo を用いる
	Darwin)
		ECHO="/bin/echo"
		;;
	*)
		ECHO="echo"
		;;
esac

$ECHO -n "ここは改行をしない"
$ECHO "メッセージです"
