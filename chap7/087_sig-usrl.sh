#/bin/sh

#実行回数カウンタ
count=0
#通信テストの対象サーバー
server="192.168.33.77"

#シグナルUSR1にトラップを設定。現在の$countを表示する
#kill -s USR1 psid により USR1シグナルを受け取った場合の処理
trap 'echo "Try Count: $count"' USR1

#ncコマンドでの連続通信テストのループ
while [ "$count" -le 1000 ]
do
	#カウンタを１増やし、nsコマンドで通信テストを行う
	#最後に１秒ウェイトを入れる
	count=$(expr $count + 1)
	nc -zv "$server" 80
	sleep 1
done
