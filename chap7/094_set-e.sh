#/bin/sh

#コマンドの終了ステータスが非ゼロの場合は、
#スクリプトをただちに終了する
set -e

#削除ファイルの格納ディレクトリ（タイプミス
deldir="/var/ololololog/dfsafsaasd"

#ディレクトリ$deldirに移動して、拡張子/logのファイルを削除する
#set -e しているため、ディレクトリ移動に失敗すればrmコマンドは実行されない
cd "$deldir"
rm -f *.log
