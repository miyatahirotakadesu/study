#/bin/sh

#環境初期化のシェル関数。ログ出力先を設定したsetting.confを読み込む
loadconf() {
	. ./setting.conf
}

#HUPシグナルの割り込みで設定を読み込みなおすように定義
trap 'loadconf' HUP

#通常起動時の、はじめの初期化を行う
loadconf

#無限ループで実行
while :
do
	#uptimeコマンドの結果を、setting.confで指定されたパスにログ出力
	uptime >> "${UPTIME_FILENAME}"
	sleep 1
done
