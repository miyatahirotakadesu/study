#/bin/sh

#3つのホストに並列してpingを実行する
#6回実行するため、それぞれ約5秒の待ち時間がかかる

ping -c 6 192.168.2.1 > host1.log &
ping -c 6 192.168.2.2 > host2.log &
ping -c 6 192.168.2.3 > host3.log &

#３つのpingが終了するまで待ち、同期をとる
wait

#pingコマンドの結果を表示する
cat host1.log host2.log host3.log
