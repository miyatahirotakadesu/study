#/bin/sh

#テスト通信先の定義
ipaddr="192.168.2.1"
port=80

#1から10までの整数値の乱数を、Perlのワンライナーで生成する
waittime=$(perl -e 'print 1 + int(rand(10))')

#テストコマンドを、ウェイトを入れて2回実行する
nc -w 5 -zv $ipaddr $port
echo "Wait: $waittime sec."
sleep $waittime
nc -w 5 -zv $ipaddr $port
