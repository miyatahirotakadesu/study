#/bin/sh

#一時ファイルを定義し、ファイルの中を空に初期化する
tmpfile="calctmp.$$"
: > "$tmpfile"

#トラップの設定。終了時に一時ファイルを削除する
trap 'rm -f "$tmpfile"' EXIT

#長い計算を行う外部スクリプトを実行する
sleep 100


