#/bin/sh

#コマンドライン引数のチェック
if [ -z "$1" ]; then
	echo "title要素を引数で指定してください" >&2
	exit
else
	#コマンドライン引数$1の文字列をtitle要素に入れて表示
	#ヒアドキュメントにハイフンを指定して
	#行頭タブを無視してインデント
	cat <<-EOT
	<html>
	<head>
		<title>$1</title>
	</head>
	
	<body>
		<p>Auth HTML Sample.</p>
	</body>
	</html>
	EOT
fi

