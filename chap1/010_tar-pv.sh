#!/bin/sh

echo "bigdata" > bigfile1.dat
echo "verybigdata" > bigfile2.dat

tar cf - bigfile1.dat bigfile2.dat | pv | gzip > archive.tar.gz
