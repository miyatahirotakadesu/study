#!/bin/sh
echo -n "Type Your Answer[y/n]: "

#現在の端末設定をシェル変数 tty_state にバックアップしてから端末をraw設定にする
tty_state=$(stty -g)
stty raw
#キーボードから１文字読む
char=$(dd bs=1 count=1 2> /dev/null)
#端末設定を元に戻す
stty "$tty_state"

echo

#入力された文字により処理分岐
case "$char" in
  [yY])
	echo "Input: YES"
	;;
  [nN])
	echo "Input: NO"
	;;
	*)
	echo "Input: What?"
	;;
esac
