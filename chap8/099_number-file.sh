#/bin/sh

url_template="http://www.example.org/download/img_%03d.jpg"

#seqコマンドで連番数値を作成する
for i in $(seq 10)
do
	url=$(printf "$url_template" $i)
	curl -0 "$url"
done
