#!/bin/sh

#データファイルの定義
datafile="/home/user1/myapp/sample.dat"

#データファイルの存在チェック
if [ -f "$datafile" ]; then
	#ifの中身は空っぽはダメ、ヌルコマンドでなんとかする
	:
else	
	echo "データファイルが存在しません: $1" >&2
	exit 1
fi
