#!/bin/sh

#抽出条件などの定義
match_id=1 #抽出するIDの値
csvfile="data.csv" #CSVファイルを指定

#CSVファイルが存在しなければ終了
if [ ! -f "$csvfile" ]; then
	echo "CSVファイルが存在しません"
	exit 1
fi

#CSVファイルの読み込み
while read line
do
	#行内の各カラムをcutコマンドで取り出す
	id=$(echo $line | cut -f 1 -d ',')
	name=$(echo $line | cut -f 2 -d ',')

	#IDカラムがシェル変数match_idで指定したIDと一致する場合は名前フィールドを表示する
	if [ "$id" -eq "$match_id" ]; then
		echo "$name"
	fi
done < "$csvfile"
