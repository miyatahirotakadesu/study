#!/bin/sh

#ダウンロードするファイルのURLパス部分、ファイル名を定義
url_path="http://www.example.org"
filename="sample.dat"

#ファイルをダウンロード。ダウンロード成功ならばmd5ハッシュ値を表示する。
#Mac/FreeBSDでは、md5sumではなくmd5コマンドを使う

curl -s0 "${url_path}${filename}" && md5sum "$filename"

#ダウンロードファイルを削除して終了する
rm -f $filename
