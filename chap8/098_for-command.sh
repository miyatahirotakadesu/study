#/bin/sh

#コマンドライン引数をチェック
if [ -z "$1" ]; then
	echo "第三オクテットまでのIPアドレスを引数として指定してください" >&2
	exit 1
fi

#対象のIPアドレスを、外部ファイルping_target.lst から
#%ADDR_HEAD%の部分に置換して順に取得する
for ipaddr in $(sed "s/%ADDR_HEAD%/$1/" ping_target.lst)
do
	#pingコマンドを実行。出力結果は不要のため /dev/nullへリダイレクト
	ping -c 1 $ipaddr > /dev/null 2>&1

	#終了ステータスで成功・失敗を表示
	if [ $? -eq 0 ]; then
		echo "[Success] ping -> $ipaddr"
	else
		echo "[Failed] ping -> $ipaddr"
	fi
done
