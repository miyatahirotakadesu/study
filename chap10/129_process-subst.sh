#/bin/sh

#比較する２つのディレクトリの定義
dir1="/var/tmp/backup1"
dir2="/var/tmp/backup2"

#commコマンドで出力を比較。
#中間ファイルを作らなくてもプロセス置換で処理出来る
comm <(ls "$dir1") <(ls "$dir2")

