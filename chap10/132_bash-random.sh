#/bin/sh

#テスト通信先の定義
ipaddr="192.168.2.1"
port=80

#1から10までの整数値の乱数を、RANDOM変数から取得する
waittime=$((RANDOM % 10 + 1))

#テストコマンドを、ウェイトを入れて２回実行する
nc -w 5 -zv $ipaddr $port
echo "Wait: $waittime sec."
sleep $waittime
nc -w 5 -zv $ipaddr $port
