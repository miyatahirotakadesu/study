#/bin/sh

url_templace="http://www.example.org/download/img_%03d.jpg"

#カウンタ変数countを整数値で宣言
declare -i count=0

while [ $count -le 10 ]
do
	url=$(printf "$url_templace" $count)
	echo $url

	#countを1増やす。exprコマンドが不要に。
	count=count+1	
done
