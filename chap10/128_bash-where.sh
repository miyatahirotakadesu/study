#/bin/sh

#探査コマンドを取得
command="$1"

#引数チェック
if [ -z "$command" ]; then
	echo "エラー：調べるコマンドを指定してください" >&2
	exit 1
fi

#環境変数$PATHのコロンをスペースに置換し、for文のループ対象リストとする
for dir in ${PATH//:/ }
do
	if [ -f "${dir}/${command}" ]; then
		echo "${dir}/${command}"
	fi
done
