#/bin/sh

#指定されたIDファイルから、「$id $status」として
#１行ずつreadコマンドで読み込む
while read id status
do
	#シェル変数idの先頭２文字が"AC"かどうかチェックする
	if [ "${id:0:2}" = "AC" ]; then
		echo "$id $status"
	fi
done < "$1"
