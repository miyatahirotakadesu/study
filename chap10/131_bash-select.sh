#/bin/sh

#メニュープロンプト文の定義
PS3='Menu: '

#メニュー表示の定義。メニューの各項目はinへのリストとして指定する。
#$itemには選択されたリストの文字列が、$REPLYには入力された数値が代入される
select item in "list file" "current directory" "exit"
do
	case "$REPLY" in
	1)
		ls
		;;
	2)
		pwd
		;;
	3)
		exit
		;;
	*)
		echo "Error: Unknown Command"
		;;
	esac
done
