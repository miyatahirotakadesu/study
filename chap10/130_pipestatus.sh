#/bin/sh

#以下のような処理を行うケースを想定する
#script.sh データ出力
#sort-data.sh データをソートする
#calc.sh 出力データの計算をする
./script.sh | ./sort-data.sh | ./calc.sh > output.txt

#別のコマンドを実行するとPIPESTATUSの値が失われるため、
#結果をコピーしておく
pipe_status=("${PIPESTATUS[@]}")

#パイプ処理の途中のコマンドの成功失敗チェック
#sort-data.shの終了ステータスが非ゼロか確認する
if [ "${pipe_status[1]}" -ne 0 ]; then
	echo "[ERROR] sort-data.sh は失敗しました" >&2
fi
