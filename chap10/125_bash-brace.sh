#/bin/sh

#bashのブレース展開{}でIPアドレスリストを作成する
for ipaddr in 192.168.2.{1..5}
do
	echo $ipaddr
	ping -c 1 "$ipaddr" > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		echo "[OK] Ping -> $ipaddr"
	else
		echo "[NG] Ping -> $ipaddr"
	fi
done
