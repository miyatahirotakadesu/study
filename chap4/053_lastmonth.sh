#/bin/sh

logdir="/var/log/myapp"

#今月15日の日付を取得する
thismonth=$(date '+%Y/%m/15 00:00:00')

#先月の日付をYYYYMMで取得する
#1 month ago は先月の同日をとるため、末日とならないように
#変数thismonthには１５日を設定した

last_YYYYMM=$(date -d "$thismonth - 1 month ago" '+%Y/%m')

#先月のログファイルをまとめてアーカイブ
tar cfv ${last_YYYYMM}.tar ${logdir}/access.log-${last_YYYYMM}*
