#!/bin/sh

day1="2012/04/01 10:49:31"
day2="2012/03/31 08:11:23"

day1_epoch=$(date -d "$day1" '+%s')
day2_epoch=$(date -d "$day2" '+%s')

echo "day1($day1): $day1_epoch"
echo "day2($day2): $day2_epoch"

echo "day interval :"
day_interval=$(expr \( $day1_epoch - $day2_epoch \) / 86400)
echo $day_interval
