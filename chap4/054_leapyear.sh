#/bin/sh

#現在の西暦を４桁で取得
declare -i mod2
year=$(date '+%Y')

logfile="/var/log/myapp/access.log-"

#西暦を割った余りを計算する
mod1=$(expr $year % 4)
mod2=$year%100
mod3=$(expr $year % 400)

#うるう年かどうかの判定
if [ $mod1 -eq 0 -a $mod2 -ne 0 -o $mod3 -eq 0 ]; then
	echo "leap year:$year"
	ls "${logfile}${year}0229"
else
	echo "not leap year:$year"
	ls "${logfile}${year}0228"
fi
