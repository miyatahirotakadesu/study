#!/bin/sh

cp largefile.tar.gz ${TMPDIR:=/tmp}
cd $TMPDIR
tar xzf largefile.tar.gz

echo "Extract files to $TMPDIR."

# := なければつくる
# :- なければつくるが、NULLならつくらない

# ${var:?message} varを返す、なければmessageを返してスクリプト終了。エラー終了的な
# : ${MYDIR:?シェル変数MYDIRがセットされていません}

# :+ 変数の存在チェック。if的に使える。phpの!emptyみたいな。
#MYDIRが空文字でなければ、flgに1をセット
#flg=${MYDIR:+1}

